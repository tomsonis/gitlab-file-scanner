package com.beben.tomasz.gitlab.file.scanner.core;

import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabProject;

import java.util.List;
import java.util.stream.Collectors;


public class GitlabFinder {

    private static final String FILE_SCAN_PATH = "projects/%s/search?scope=blobs&search=%s";
    private static final String PROJECT_PATH = "projects?search=%s";

    private final GitlabAPI gitlabAPI;

    private GitlabFinder(String gitlabUrl, String gitlabPrivateToken) {
        this.gitlabAPI = GitlabAPI.connect(gitlabUrl, gitlabPrivateToken);
    }

    public static GitlabFinder of(String gitlabUrl, String gitlabPrivateToken) {
        return new GitlabFinder(gitlabUrl, gitlabPrivateToken);
    }

    public List<GitlabFullSearchResult> search(String projectPrefix, String encodedSearchText) {
        return findAllProjectByPrefix(projectPrefix)
                .parallelStream()
                .map(gitlabProject -> getGitlabFullSearchResult(gitlabProject, encodedSearchText))
                .filter(gitlabFullSearchResult -> !gitlabFullSearchResult.getGitlabFileSearchResult().isEmpty())
                .collect(Collectors.toList());
    }

    private GitlabFullSearchResult getGitlabFullSearchResult(GitlabProject gitlabProject, String encodedSearchText) {
        String tailUrl = String.format(
                FILE_SCAN_PATH,
                String.valueOf(gitlabProject.getId()),
                encodedSearchText
        );

        List<GitlabFileSearchResult> fileSearchResults = gitlabAPI.retrieve().getAll(
                tailUrl,
                GitlabFileSearchResult[].class
        );

        return GitlabFullSearchResult.of(
                fileSearchResults,
                gitlabProject
        );
    }

    private List<GitlabProject> findAllProjectByPrefix(String projectPrefix) {
        return gitlabAPI.retrieve().getAll(
                String.format(
                        PROJECT_PATH,
                        projectPrefix
                ),
                GitlabProject[].class
        );
    }
}
