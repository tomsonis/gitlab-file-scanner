package com.beben.tomasz.gitlab.file.scanner.gui.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@NoArgsConstructor(staticName = "newInstance")
@AllArgsConstructor(staticName = "of")
public class GitlabConnectionModel implements Serializable {

    @URL(message = "Gitlab URL must be a valid URL")
    @NotBlank(message = "Gitlab URL must not be blank.")
    private String gitlabUrl;

    @NotBlank(message = "Gitlab Private Token must not be blank.")
    private String gitlabPrivateToken;
}
