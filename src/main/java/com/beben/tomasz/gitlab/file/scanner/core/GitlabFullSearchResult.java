package com.beben.tomasz.gitlab.file.scanner.core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.gitlab.api.models.GitlabProject;

import java.util.List;

@Getter
@AllArgsConstructor(staticName = "of")
public class GitlabFullSearchResult {

    private List<GitlabFileSearchResult> gitlabFileSearchResult;
    private GitlabProject gitlabProject;

    @Override
    public String toString() {
        return "\n{" +
                "\n\tprojectName=" + gitlabProject.getName() +
                "\n\tfileNames=" + gitlabFileSearchResult +
                "\n}";
    }
}
