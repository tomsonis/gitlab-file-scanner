package com.beben.tomasz.gitlab.file.scanner.gui.service;

import com.beben.tomasz.gitlab.file.scanner.gui.utils.Validator;
import com.beben.tomasz.gitlab.file.scanner.gui.model.GitlabConnectionModel;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintViolation;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

@NoArgsConstructor(staticName = "newInstance")
public class PropertiesFileReader {

	private static final String GITLAB_URL = "gitlab.url";
	private static final String GITLAB_PRIVATE_TOKEN = "gitlab.private_token";

	private GitlabConnectionModel gitlabConnectionModel;

	private Set<ConstraintViolation<GitlabConnectionModel>> constraintViolations;

	public static PropertiesFileReader readProperties(File file) throws IOException {
		PropertiesFileReader propertiesFileReader = new PropertiesFileReader();

		if (Objects.nonNull(file)) {
			Properties properties = new Properties();
			properties.load(new FileReader(file));

			propertiesFileReader.gitlabConnectionModel = GitlabConnectionModel.of(
					properties.getProperty(GITLAB_URL),
					properties.getProperty(GITLAB_PRIVATE_TOKEN)
			);
			propertiesFileReader.constraintViolations = Validator.validate(
					propertiesFileReader.gitlabConnectionModel
			);
			return propertiesFileReader;
		}

		throw new IllegalArgumentException("Can't load properties file.");
	}

	public boolean hasNoError() {
		return Objects.nonNull(constraintViolations) && constraintViolations.isEmpty();
	}

	public GitlabConnectionModel getGitlabConnectionModel() {
		return gitlabConnectionModel;
	}

	public Set<ConstraintViolation<GitlabConnectionModel>> getConstraintViolations() {
		return constraintViolations;
	}
}
