package com.beben.tomasz.gitlab.file.scanner.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GitlabFileSearchResult {

    private String basename;

    private String data;

    private String filename;

    private String id;

    private String ref;

    private String startline;

    @JsonProperty("project_id")
    private String projectId;

    @Override
    public String toString() {
        return "\n\t\tfilename='" + filename + '\'';
    }
}
