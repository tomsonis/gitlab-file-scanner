package com.beben.tomasz.gitlab.file.scanner.gui.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor(staticName = "of")
public class GitlabSearchModel {

    private String projectPrefix;

    @NotBlank(message = "Search Text must not be blank.")
    private String searchText;
}
