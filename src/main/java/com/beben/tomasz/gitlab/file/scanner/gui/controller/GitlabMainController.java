package com.beben.tomasz.gitlab.file.scanner.gui.controller;

import com.beben.tomasz.gitlab.file.scanner.core.GitlabFinder;
import com.beben.tomasz.gitlab.file.scanner.core.GitlabFullSearchResult;
import com.beben.tomasz.gitlab.file.scanner.gui.model.GitlabConnectionModel;
import com.beben.tomasz.gitlab.file.scanner.gui.model.GitlabSearchModel;
import com.beben.tomasz.gitlab.file.scanner.gui.service.PropertiesFileReader;
import com.beben.tomasz.gitlab.file.scanner.gui.utils.Validator;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import javax.validation.ConstraintViolation;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class GitlabMainController {

	@FXML
	private TextField gitlabUrl;

	@FXML
	private TextField gitlabPrivateToken;

	@FXML
	private TextField projectPrefix;

	@FXML
	private TextField searchText;

	@FXML
	private Button searchButton;

	@FXML
	private TextField filePropertiesPath;

	@FXML
	private Button chooseFileButton;

	@FXML
	private TextArea searchResult;

	private GitlabFinder gitlabFinder;

	@FXML
	public void initialize() throws IOException {
		Path path = Paths.get(".");
		List<Path> collect = Files.walk(path)
				.filter(propertiesPath -> propertiesPath.toString().endsWith(".properties"))
				.collect(Collectors.toList());

		for (Path propertiesPath : collect) {
			try {
				File file = propertiesPath.toFile();
				PropertiesFileReader propertiesFileReader = PropertiesFileReader.readProperties(file);
				if (propertiesFileReader.hasNoError()) {
					applyProperties(propertiesFileReader.getGitlabConnectionModel(), file.getPath());
					break;
				}
			} catch (IOException ignored) {
			}
		}
	}

	@FXML
	public void onFileChoose() {
		try {
			FileChooser fileChooser = prepareFileChooser();
			File file = fileChooser.showOpenDialog(chooseFileButton.getScene().getWindow());

			PropertiesFileReader propertiesFileReader = PropertiesFileReader.readProperties(file);
			if (propertiesFileReader.hasNoError()) {
				applyProperties(propertiesFileReader.getGitlabConnectionModel(), file.getPath());
			} else {
				displayConstraintValidationErrors(propertiesFileReader.getConstraintViolations());
			}
		} catch (IOException e) {
			displayErrors(e.getMessage());
		}
	}

	@FXML
	public void search() {
		GitlabSearchModel gitlabSearchModel = GitlabSearchModel.of(
				projectPrefix.getText(),
				searchText.getText()
		);
		Set<ConstraintViolation<GitlabSearchModel>> constraintViolations = Validator.validate(gitlabSearchModel);

		if (constraintViolations.isEmpty()) {
			changeDisableSearchPropertyTo(true);

			Thread thread = new Thread(() -> {
				try {
					List<GitlabFullSearchResult> search = gitlabFinder.search(
							gitlabSearchModel.getProjectPrefix(),
							encodeSearchText(gitlabSearchModel.getSearchText())
					);
					searchResult.setText(search.toString());
				} catch (Exception e) {
					Platform.runLater(() -> displayErrors(e.getMessage()));
				}

				changeDisableSearchPropertyTo(false);
			});
			thread.start();
			thread.setName("Gitlab searching.");
		} else {
			displayConstraintValidationErrors(constraintViolations);
			changeDisableSearchPropertyTo(false);
		}
	}

	private void applyProperties(GitlabConnectionModel gitlabConnectionModel, String filePath) {
		filePropertiesPath.setText(filePath);
		gitlabUrl.setText(gitlabConnectionModel.getGitlabUrl());
		gitlabPrivateToken.setText(gitlabConnectionModel.getGitlabPrivateToken());

		gitlabFinder = GitlabFinder.of(
				gitlabUrl.getText(),
				gitlabPrivateToken.getText()
		);
		changeDisableSearchPropertyTo(false);
	}

	private FileChooser prepareFileChooser() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Loading .properties file");

		FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Properties file", "*.properties");
		fileChooser.getExtensionFilters().add(extensionFilter);
		return fileChooser;
	}


	private <T> void displayConstraintValidationErrors(Set<ConstraintViolation<T>> constraintViolations) {
		String errorMessages = constraintViolations.stream()
				.map(ConstraintViolation::getMessage)
				.collect(Collectors.joining("\n"));

		displayErrors(errorMessages);
	}

	private void displayErrors(String errorMessages) {
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle("Errors");
		alert.setHeaderText(null);
		alert.setContentText(errorMessages);
		alert.showAndWait();
	}

	private static String encodeSearchText(String searchText) throws UnsupportedEncodingException {
		String encode = URLEncoder.encode(searchText, StandardCharsets.UTF_8.name());
		return encode.replace("+", "%20");
	}

	private void changeDisableSearchPropertyTo(boolean status) {
		projectPrefix.setDisable(status);
		searchText.setDisable(status);
		searchButton.setDisable(status);
		filePropertiesPath.setDisable(status);
		chooseFileButton.setDisable(status);
	}
}
