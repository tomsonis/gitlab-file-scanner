package com.beben.tomasz.gitlab.file.scanner.gui.utils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class Validator {

	private static final ValidatorFactory VALIDATOR_FACTORY = Validation.buildDefaultValidatorFactory();
	private static final javax.validation.Validator validator = VALIDATOR_FACTORY.getValidator();

	public static <T> Set<ConstraintViolation<T>> validate(T model) {
		return validator.validate(model);
	}
}
